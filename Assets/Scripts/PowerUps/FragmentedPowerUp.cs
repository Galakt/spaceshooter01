﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FragmentedPowerUp : PowerUp {
	new void Update () {
		base.Update();
	}

	public new void OnTriggerEnter2D(Collider2D other){
		base.OnTriggerEnter2D(other);
		if ("Player" == other.tag) {
			Weapons weapons = GameObject.Find("Weapons").GetComponent<Weapons>();
			weapons.bulletPrefab = Resources.Load<GameObject>("Prefabs/Bullets/Fragmented");
			weapons.type = "fragmented";
			weapons.ammunition = 3;

			Destroy(gameObject);
		}
	}
}
