﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour {
	public float speed;
	
	public void Update () {
		transform.Translate (0, speed * Time.deltaTime, 0);
	}

    public void OnTriggerEnter2D(Collider2D other){
		if ("Finish" == other.tag) {
			Destroy(gameObject);
		}
	}
}
