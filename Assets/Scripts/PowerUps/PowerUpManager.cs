﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpManager : MonoBehaviour {
    // Temps entre spawns
	public float timeToLaunch;
    // Prefabs per instanciar enemics
	public GameObject[] prefabs;

	private float currentTime;
	private static PowerUpManager instance;

	void Start () {
		if (null == instance)
        {
			instance = this;
		}

		prefabs = Resources.LoadAll<GameObject>("Prefabs/PowerUps");

		timeToLaunch = Random.Range(6f,8f);
	}

	void Update () {
		timeToLaunch -= Time.deltaTime;

		if (timeToLaunch <= 0)
        {
            this.LaunchPowerUp();
			timeToLaunch = Random.Range(6f,8f);
		}
	}

    public static PowerUpManager getInstance()
    {
        return instance;
    }

    public void LaunchPowerUp()
    {
    	Vector3 xVariation = transform.position + new Vector3(Random.Range(-7,8),0,0);
    	GameObject powerUp = Instantiate(prefabs[Random.Range(0,prefabs.Length)], xVariation, Quaternion.identity);
    	powerUp.transform.Rotate(new Vector3(0,0,180));       
    }

    public void Reset() {
        GameObject[] powerUps = GameObject.FindGameObjectsWithTag("PowerUp");
        for (int i = 0; i < powerUps.Length; i++) {
            Destroy(powerUps[i]);
        }
    }
}
