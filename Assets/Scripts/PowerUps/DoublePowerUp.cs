﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoublePowerUp : PowerUp {
	new void Update () {
		base.Update();
	}

	public new void OnTriggerEnter2D(Collider2D other){
		base.OnTriggerEnter2D(other);
		if ("Player" == other.tag) {
			Weapons weapons = GameObject.Find("Weapons").GetComponent<Weapons>();
			weapons.bulletPrefab = Resources.Load<GameObject>("Prefabs/Bullets/Double");
			weapons.type = "double";
			weapons.ammunition = 10;

			Destroy(gameObject);
		}
	}
}
