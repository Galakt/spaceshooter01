﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosiveBullet : Bullet{
    public int timeToExplode = 1;
    public float currentTime = 0f;

    protected new virtual void Update()
    {
        base.Update();
            
        currentTime += Time.deltaTime;
        if (currentTime > timeToExplode) {
            this.ShotMiniBullets();
        }
    }

    void ShotMiniBullets()
    {
        float z = transform.rotation.eulerAngles.z;
        GameObject miniBullet = Resources.Load<GameObject>("Prefabs/Bullets/MiniExplosive");
        int[] rotations = new int[] {-45, 45, -135, 135, -90, 90, -180, 0};
        for (int i = 0; i < rotations.Length; i++) {
            Instantiate(miniBullet, transform.position,  Quaternion.Euler(0, 0, rotations[i] + z));
        }

        Destroy(gameObject);
    }
}
