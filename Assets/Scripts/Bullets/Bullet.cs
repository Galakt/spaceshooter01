﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Bullet : MonoBehaviour {
	public float speed;
	
	protected void Update () {
		transform.Translate (0, speed * Time.deltaTime, 0);
	}

	public void OnTriggerEnter2D(Collider2D other){
		if ("Finish" == other.tag || "TniyMeteor" == other.tag || "Meteor" == other.tag || "Enemy" == other.tag) {
			Destroy(gameObject);
		}
	}
}
