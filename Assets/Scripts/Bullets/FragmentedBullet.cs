﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FragmentedBullet : Bullet {
    public float timeToExplode = 0.5f;
    public float currentTime = 0f;

    protected new virtual void Update()
    {
        base.Update();
            
        currentTime += Time.deltaTime;
        if (currentTime > timeToExplode) {
            this.ShotMiniBullets();
        }
    }

    void ShotMiniBullets()
    {
        float z = transform.rotation.eulerAngles.z;
        GameObject miniBullet = Resources.Load<GameObject>("Prefabs/Bullets/Fragmented");
        int[] rotations = new int[] {-10, 10};
        for (int i = 0; i < rotations.Length; i++) {
            Instantiate(miniBullet, transform.position,  Quaternion.Euler(0, 0, rotations[i] + z));
        }

        Destroy(gameObject);
    }
}
