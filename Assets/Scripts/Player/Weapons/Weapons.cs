﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapons : MonoBehaviour
{
	private Cannon mainCannon;
	private Cannon leftCannon;
	private Cannon rightCannon;

	public int ammunition;
	public string type;
	public GameObject bulletPrefab;
	public float timeCounter = 0;
	public float cooldown;

	private void Start()
	{
		this.SetBaseBullets();

		mainCannon = GameObject.Find("MainCannon").GetComponent<Cannon>();
		leftCannon = GameObject.Find("LeftCannon").GetComponent<Cannon>();
		rightCannon = GameObject.Find("RightCannon").GetComponent<Cannon>();
	}

	void Update(){	
		timeCounter += Time.deltaTime;
	}

	public void ShotWeapon()
	{
		string state = GameObject.Find("PlayerShip").GetComponent<PlayerBehaviour>().state;
		if (timeCounter >= cooldown && state != "dead") {
			if (ammunition > 0) {
				if ("double" == type) {
					leftCannon.Shot(bulletPrefab);
					rightCannon.Shot(bulletPrefab);
				} else {
					mainCannon.Shot(bulletPrefab);
				}
				--ammunition;
			} else {
				this.SetBaseBullets();
				mainCannon.Shot(bulletPrefab);
			}
			
			timeCounter = 0f;
		}
	}

	public void SetBaseBullets()
	{
		bulletPrefab = Resources.Load<GameObject>("Prefabs/Bullets/Normal");
		type = "normal";
		ammunition = -1;
	}
}
