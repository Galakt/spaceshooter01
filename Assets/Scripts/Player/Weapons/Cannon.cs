using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour {
	public AudioSource audioShoot;

	public void Shot(GameObject bulletPrefab){
		Instantiate(bulletPrefab, transform.position, Quaternion.identity);
		audioShoot.Play ();
	}
}
