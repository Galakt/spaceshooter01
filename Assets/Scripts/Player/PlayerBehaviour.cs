﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour {
	public float speed;
	public AudioSource audioExplosion;
	public ParticleSystem particle;
	public Vector2 limits;
	public Propeller propeller;
	public GameObject graphics;
	public Vector3 iniPos;
	public string state;

	private BoxCollider2D myCollider;
	private Vector2 axis;

	void Awake (){
		myCollider = GetComponent<BoxCollider2D> ();
	}

	// Update is called once per frame
	void Update () {
		transform.Translate (axis * speed * Time.deltaTime);

		if (transform.position.x > limits.x) {
			transform.position = new Vector3 (limits.x, transform.position.y, transform.position.z);
		}else if (transform.position.x < -limits.x) {
			transform.position = new Vector3 (-limits.x, transform.position.y, transform.position.z);
		}

		if (transform.position.y > limits.y) {
			transform.position = new Vector3 (transform.position.x, limits.y, transform.position.z);
		}else if (transform.position.y < -limits.y) {
			transform.position = new Vector3 (transform.position.x, -limits.y, transform.position.z);
		}

		if (axis.y > 0) {
			propeller.RedFire ();
		} else if (axis.y < 0) {
			propeller.BlueFire ();
		} else {
			propeller.Stop ();
		}
	}

	private void Explode (){
		MyGameManager.getInstance ().LoseLive ();
		graphics.SetActive (false);
		myCollider.enabled = false;
		audioExplosion.Play ();
		Invoke ("Reset", 2);
		particle.Play ();
	}

	protected void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Meteor" || "EnemyBullet" == other.tag || "Enemy" == other.tag) {
			state = "dead";
			Explode ();
        }
	}

	public void Reset (){
		transform.position = iniPos;
		graphics.SetActive (true);
		myCollider.enabled = true;
		state = "alive";
	}

	public void SetAxis(Vector2 currentAxis){
		axis = currentAxis;
	}
}
