﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour {

		public GameObject pausePanel;
		public GameObject gameoverPanel;
		public GameObject gameplayPanel;
		public GameObject creditPanel;
		public GameObject titlePanel;
		public GameObject startButton;
		public GameObject creditButton;
		public GameObject backCreditButton;
		public GameObject retryButton;
		public GameObject exitButton;
		public GameObject resumeButton;
		public GameObject logoPanel; 
		public Text highScoreText;

		public void OpenPausePanel() { pausePanel.SetActive(true); }
		public void ClosePausePanel() { pausePanel.SetActive(false); }

		public void OpenGameoverPanel() { gameoverPanel.SetActive(true); }
		public void CloseGameoverPanel() { gameoverPanel.SetActive(false); }

		public void OpenGameplayPanel() { gameplayPanel.SetActive(true); }
		public void CloseGameplayPanel() { gameplayPanel.SetActive(false); }

		public void OpenTitlePanel() { titlePanel.SetActive(true); }
		public void CloseTitlePanel() { titlePanel.SetActive(false); }

		public void OpenLogoPanel() {logoPanel.SetActive(true);}
		public void CloseLogoPanel() {logoPanel.SetActive(false);}

		public void OpenCreditPanel() {creditPanel.SetActive(true);}
		public void CloseCreditPanel() {creditPanel.SetActive(false);}
}
