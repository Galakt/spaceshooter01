﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MyGameManager : MonoBehaviour {

	public Text highscoreText;
	public Text livesText;
	public bool isPaused;
	public bool gameover;
	public bool startGame = false;    

	private static MyGameManager instance; 
	private int lives;
	public int highscore;
	private HUD hud;
	public float logoTime = 3f;
	public string type = "logo";

	void Awake () {
		if (instance == null) {
			instance = this;
		}
	}

	public static MyGameManager getInstance(){
		return instance;
	}

	void Start () {
		isPaused = false;
		startGame = false;

		hud = GameObject.FindGameObjectWithTag("HUD").GetComponent<HUD>();
		hud.ClosePausePanel ();
		hud.CloseGameoverPanel ();
		hud.CloseGameplayPanel ();
		hud.CloseTitlePanel ();
		hud.OpenLogoPanel ();
   
   		Time.timeScale = 0;
		lives = 3;
		highscoreText.text = highscore.ToString("D5");
		livesText.text = "x" + lives.ToString();
	}

	void Update () {
		// Logo
		if ("logo" == type && Time.realtimeSinceStartup > 3) {
			hud.CloseLogoPanel();
			hud.OpenTitlePanel();
			type = "title";

			return;
		}

		if ("game" == type && 0 >= lives) {
			this.GameOver();
			type = "gameover";

			return;
		}

		if (Input.GetKeyUp("p")) {
			this.PauseGame();
		}

		if (Input.GetKeyUp("escape")) {
			this.Exit();
		}
	}

	public void LoseLive() {
		lives--;
		livesText.text = "x" + lives.ToString ();
	}

	public void PauseGame() {
		isPaused = !isPaused;

		if(isPaused) {
			Time.timeScale = 0;
			hud.OpenPausePanel();
		} else {
			Time.timeScale = 1;
			hud.ClosePausePanel();
		}
	}

	public void StartGame() {
		hud.CloseTitlePanel();
		hud.OpenGameplayPanel();
		Time.timeScale = 1;
		type = "game";
	}
		
	public void GameOver() {
		this.Restart();
		Time.timeScale = 0;
		hud.CloseGameplayPanel();
		hud.OpenGameoverPanel();
		hud.highScoreText.text = highscore.ToString();
	}

	public void retry() {
		lives = 3;
		highscore = 0;
		highscoreText.text = highscore.ToString("D5");
		livesText.text = "x" + lives.ToString();
		hud.CloseGameoverPanel();
		hud.OpenGameplayPanel();
		Time.timeScale = 1;
		type = "game";
	}

	public void Restart() {
		GameObject.Find("PlayerShip").GetComponent<PlayerBehaviour>().Reset();
		GameObject.Find("Meteors").GetComponent<MeteorManager>().Reset();
		GameObject.Find("Enemies").GetComponent<EnemyManager>().Reset();
		GameObject.Find("PowerUps").GetComponent<PowerUpManager>().Reset();

		GameObject[] bullets = GameObject.FindGameObjectsWithTag("Bullet");
        for (int i = 0; i < bullets.Length; i++) {
            Destroy(bullets[i]);
        }

        GameObject[] enemyBullets = GameObject.FindGameObjectsWithTag("EnemyBullet");
        for (int i = 0; i < enemyBullets.Length; i++) {
            Destroy(enemyBullets[i]);
        }
	}

	public void Credits() {
		hud.CloseTitlePanel();
		hud.OpenCreditPanel();
	}

	public void ExitCredits() {
		hud.CloseCreditPanel();
		hud.OpenTitlePanel();
	}

	public void Exit() {
		Application.Quit();
	}
}
