﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2 : Enemy
{
    private Transform target;

    new void Start()
    {
        base.Start();

        target = GameObject.Find("PlayerShip").transform;
    }

    new void Update()
    {
        if (transform.position.y <= 3)
        {
            moveSpeed.y = 0;
        }
        base.Update();

        Vector3 dir = target.transform.position - transform.position;
        float angle = (Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg)-90;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime*.5f);
    }

    protected override void Shoot()
    {
        if ("dead" != state) {
            base.Shoot();
            GameObject bullet = Instantiate(bulletPrefab, transform.position, Quaternion.identity);
            bullet.GetComponent<EnemyBullet>().direccio = target.transform.position - bullet.transform.position;
            bullet.GetComponent<AudioSource>().Play();
        }
    }

    protected new void OnTriggerEnter2D(Collider2D other)
    {
        base.OnTriggerEnter2D(other);
    }

    private void OnDestroy()
    {
        try {
            --GameObject.Find("Enemies").GetComponent<EnemyManager>().actualEnemy2;
        } catch {
            return;
        }
    }
}
