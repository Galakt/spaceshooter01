﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy1 : Enemy
{
    new void Start()
    {
        base.Start();
    }

    new void Update()
    {
        base.Update();
    }

    protected override void Shoot()
    {
        if ("dead" != state) {
            GameObject bullet = Instantiate(bulletPrefab, transform.position, Quaternion.identity);
            bullet.transform.Rotate(new Vector3(0, 0, 180));
            bullet.GetComponent<EnemyBullet>().direccio = new Vector3(0, 1, 0);
            bullet.GetComponent<AudioSource>().Play();
        }
    }

    protected new void OnTriggerEnter2D(Collider2D other)
    {
        base.OnTriggerEnter2D(other);
    }
}
