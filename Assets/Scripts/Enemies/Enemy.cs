﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {
	public Vector2 moveSpeed;
    public float shootTime;
    public GameObject bulletPrefab;
    public ParticleSystem particle;
    public AudioSource explosion;
    public string state;

    private float currentTime;
    private Collider2D enemyCollider;

    public void Start () {
        enemyCollider = GetComponent<Collider2D>();
        enemyCollider.enabled = true;
    }
    	
	public void Update () {
		transform.Translate (moveSpeed.x * Time.deltaTime, moveSpeed.y * Time.deltaTime, 0);

        currentTime += Time.deltaTime;
        if (currentTime > shootTime)
        {
            Shoot();
            currentTime -= shootTime;
        }
	}

    protected virtual void Shoot() {}

    protected void OnTriggerEnter2D(Collider2D other)
    {
        if ("PlayerShip" == other.tag || "Bullet" == other.tag)
        {
            state = "dead";
            GameObject.Find("GameManager").GetComponent<MyGameManager>().highscore += 20;
            GameObject.Find("GameManager").GetComponent<MyGameManager>().highscoreText.text = GameObject.Find("GameManager").GetComponent<MyGameManager>().highscore.ToString("D5");
            explosion.Play();
            particle.Play();
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
            gameObject.GetComponent<Collider2D>().enabled = false;
            Invoke("DestroyThis", 1f);
        } else if ("Finish" == other.tag) {
            Destroy(gameObject);
        }
    }

    public void DestroyThis () {
        Destroy(gameObject);
    }
}
