﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{
    public float speed;
    public Transform target;
    public Vector3 direccio;

    private Collider2D bulletCollider;

    void Start()
    {
        target = GameObject.Find("PlayerShip").transform;
        bulletCollider = GetComponent<Collider2D>();
        bulletCollider.enabled = true;
    }

    protected void Update()
    {
        Vector3 dir = direccio;
        dir.Normalize();
        dir *= speed * Time.deltaTime;
        transform.Translate(dir);
    }
    
    public void OnTriggerEnter2D(Collider2D other)
    {
        if ("Player" == other.tag || "Finish" == other.tag)
        {
            Destroy(gameObject);
        }
    }
}
