﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour {
    // Temps entre spawns
	public float timeToLaunch;
    // Prefabs per instanciar enemics
	public GameObject[] enemyPrefabs;
    // Nombre màxim d'instàncies del tipus 2 in-game
    public int maxEnemy2;
    // Nombre actual d'instàncies del tipus 2 in-game
    public int actualEnemy2 = 0;

	private float currentTime;
	private static EnemyManager instance;

    GameObject enemic;

	void Start () {
		if (null == instance) {
			instance = this;
		}
	}

	void Update () {
		currentTime += Time.deltaTime;

		if (currentTime > timeToLaunch) {
            this.LaunchEnemy();
			currentTime = 0f;
		}
	}

    public static EnemyManager getInstance()
    {
        return instance;
    }

    public void LaunchEnemy()
    {
        GameObject prefab;
        if (actualEnemy2 == maxEnemy2) {
            prefab = enemyPrefabs[0];
        } else {
            // Si és passen ints, el paràmetre max és exclusiu, per tant el valors retornats poden ser nomès 0 o 1
            int tipus = Random.Range(0, 2);
            if (1 == tipus) {
                actualEnemy2++;
            }

            prefab = enemyPrefabs[tipus];
        }
        Vector3 xVariation = transform.position + new Vector3(Random.Range(-7,8),0,0);
        enemic = Instantiate(prefab, xVariation, Quaternion.identity);
        enemic.transform.Rotate(new Vector3(0,0,180));        
    }

    public void Reset() {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        for (int i = 0; i < enemies.Length; i++) {
            Destroy(enemies[i]);
        }
    }
}
