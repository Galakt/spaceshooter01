﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorTiny : Meteor {
	public GameObject [] customGraphics;

	// Use this for initialization
	void Awake () {

		int selected = Random.Range (0, customGraphics.Length);
		for (int i=0;i<customGraphics.Length;i++){
			if (i !=selected){
				customGraphics [i].SetActive (false);
		
	}
}
		enabled = true;
	}
	public new void OnTriggerEnter2D (Collider2D other)
	{
		if (other.tag == "Meteor") {
			Explode ();
		}else if (other.tag == "Bullet"){
			Explode ();
	}
}
}

