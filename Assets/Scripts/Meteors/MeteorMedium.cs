﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorMedium : Meteor {
	public GameObject [] customGraphics;

	// Use this for initialization
	void Awake () {

		int selected = Random.Range (0, customGraphics.Length);
		for (int i=0;i<customGraphics.Length;i++){
			if (i !=selected){
				customGraphics [i].SetActive (false);
		
	}
}
		enabled = true;
	}
	protected override void Explode (){
		MeteorManager.getInstance ().LaunchMeteor (0, transform.position, new Vector2 (-4, -4), -5);
		MeteorManager.getInstance ().LaunchMeteor (0, transform.position, new Vector2 (4, -4), -5);

		base.Explode ();
	}
}
