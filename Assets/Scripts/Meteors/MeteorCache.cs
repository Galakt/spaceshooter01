﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorCache
{
	private Meteor[] meteors;
	private int currentMeteor = 0;

	public MeteorCache(GameObject prefabMeteor, Vector3 position, Transform parent, int numMeteors)
	{
		meteors = new Meteor[numMeteors];

		Vector3 tmpPosition = position;
		for(int i = 0; i < numMeteors; i++)
		{
			meteors [i] = GameObject.Instantiate (prefabMeteor, tmpPosition, Quaternion.identity, parent).GetComponent<Meteor>();
			meteors [i].name = prefabMeteor.name+"_meteor_" + i;
			tmpPosition.x += 1;
		}
	} 

	public Meteor GetMeteor()
	{
		if(currentMeteor >meteors.Length - 1){
			currentMeteor = 0;
		}
		return meteors [currentMeteor++];
	}

	public void Reset() {
		for(int i=0; i < meteors.Length; i++) {
			meteors[i].Reset();
		}
	}
}