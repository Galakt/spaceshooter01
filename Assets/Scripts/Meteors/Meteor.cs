﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor : MonoBehaviour {

	public bool launched;
	public Vector2 moveSpeed;
	protected float rotationSpeed;

	public ParticleSystem particle;
	public GameObject graphicsMeteor;
	private Collider2D meteorCollider;

	private Vector3 inipos;

	public AudioSource audioExplosion;

	// Use this for initialization
	void Start () {
		inipos = transform.position;
		meteorCollider = GetComponent<Collider2D> ();
	}

	public void LaunchMeteor (Vector3 position, Vector2 direction, float rotation){
		transform.position = position;
		launched = true;
		transform.GetChild(0).Rotate (0,0, rotationSpeed * Time.deltaTime);
		moveSpeed = direction;
		rotationSpeed = rotation;
	}
	// Update is called once per frame
	void Update () {
		if (launched){
			transform.Translate (moveSpeed.x * Time.deltaTime, moveSpeed.y * Time.deltaTime, 0);
			transform.GetChild(0).Rotate (0, 0, rotationSpeed);
		}
	}
	protected void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Bullet") {
			GameObject.Find("GameManager").GetComponent<MyGameManager>().highscore += 10;
			GameObject.Find("GameManager").GetComponent<MyGameManager>().highscoreText.text = GameObject.Find("GameManager").GetComponent<MyGameManager>().highscore.ToString("D5");
			Explode ();
		}if (other.tag == "TniyMeteor"){
			Explode ();
		}else if (other.tag == "Finish"){
			Reset ();
		}
	}

	public void Reset(){
		transform.position = inipos;
		launched = false;
		graphicsMeteor.SetActive (true);
		meteorCollider.enabled = true;
	}

	protected virtual void Explode (){
		audioExplosion.Play ();
		meteorCollider.enabled = false;
		launched = false;
		graphicsMeteor.SetActive (false);
		particle.Play ();
		Invoke ("Reset", 1);
	}
}
